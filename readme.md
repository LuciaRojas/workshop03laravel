# Workshop 03 - Laravel From The Scratch (2° Entregable)Tareaa


Primero que todo se levanta  vagrant con el siguiente comando `vagrant up` 


luego se el siguiente comando `vagrant rsync-auto`  

Luego entraremos donde tenemos el proyecto y lo abrimos para trabajar 
 
![coamdo tree](01.jpg "comando tree")



# Laravel From The Scratch - 6. Controllers Techniques
## 26.Leverage Route Model Binding

Hemos estado obteniendo manualmente un registro de la base de datos usando un comodín del URI. Sin embargo, Laravel puede realizar esta consulta por nosotros automáticamente, gracias al enlace del modelo de ruta.

En este video nos muestra que ya no hace pasarl el valor el id del valor, si no ahora lo que pasaremos por parametro es el arreglo de `articulo` donde ahora a todas las funciones se le cambiaran el parametro por el arreglo 
![coamdo tree](02.jpg "comando tree")
![coamdo tree](03.jpg "comando tree")
![coamdo tree](04.jpg "comando tree")



## 27.Reduce Duplication
Su siguiente técnica es reducir la duplicación. Si revisa nuestro CurrentArticlesController, hacemos referencia a las claves de solicitud en varios lugares. Ahora bien, resulta que hay una forma útil de reducir considerablemente esta repetición

Vamos hacer lo siguiente para validar los valores requeridos 
![coamdo tree](05.jpg "comando tree")
En el modelo de articulo creamos lo siguiente  donde a la variable la declaramos  `protected`la cual proteger esa propiedad del establecedor, asegurándose así de que siempre contendrá el tipo de variable que espera que tenga
![coamdo tree](06.jpg "comando tree")
![coamdo tree](07.jpg "comando tree")
se crea una funcion `protected` alcance cuando desee que su variable / función sea visible en todas las clases que amplían la clase actual, incluida la clase principal
![coamdo tree](08.jpg "comando tree")
pondremos esa funcion que creamos en el de actualiazr para que cuando va antes al formulario de actualizar verificar que van los datos
![coamdo tree](09.jpg "comando tree")


## 28.Consider Named Routes
Las rutas con nombre le permiten traducir un URI en una variable. De esta manera, si una ruta cambia en algún punto del camino, todos sus enlaces se actualizarán automáticamente, debido al hecho de que hacen referencia a la versión nombrada de la ruta en lugar de la ruta codificada.

Nos dirijimos a las rutas donde cambairemos la de ver articulos 
![coamdo tree](10.jpg "comando tree")
en la vista tambien cambiaremos la ruta que ahora lo utilizaremos de la siguiente manera
![coamdo tree](11.jpg "comando tree")
y ahora en el controler tambien vamos a cambiar la ruta para cuando va al formulario de actualizar 
![coamdo tree](12.jpg "comando tree")
A este tambien la cambiaremos la forma de la ruta
![coamdo tree](13.jpg "comando tree")
Y tambien cambiamos la ruta del articulo
![coamdo tree](14.jpg "comando tree")
creamos una funcion nueva que se llamara `path`
![coamdo tree](15.jpg "comando tree")
y a la hora de actualizar cambiaremos 
![coamdo tree](16.jpg "comando tree")
y ahora tambien cambiaremos la ruta
![coamdo tree](17.jpg "comando tree")


# Laravel From The Scratch - 7 Eloquent
## 29.Basic Eloquent Relationships

Volvamos ahora a Eloquent y comencemos a discutir las relaciones. Por ejemplo, si tengo una instancia de $user, ¿cómo puedo recuperar todos los proyectos creados por ese usuario? O si, en cambio, tengo una instancia de $project, ¿cómo buscaría al usuario que administra ese proyecto? Para una inmersión más profunda, revise la serie Laracasts de Eloquent Relationships.

Para eso nos dirijimos a los archivos que tenemos ya 
![coamdo tree](18.jpg "comando tree")
Vamos al archivo de `Project` donde crearemos un a functin con el nombre user el cual retornaremos un valor;
![coamdo tree](19.jpg "comando tree")
Luego en el archivo user  vamos a crear las dos siguientes funciones.
![coamdo tree](20.jpg "comando tree")

## 30.Understanding Foreign Keys and Database Factories

Si un artículo está asociado con un usuario, entonces debemos agregar la clave externa necesaria y los métodos de relación. Sin embargo, como parte de esto, también revisaremos rápidamente las fábricas de bases de datos y su utilidad durante la fase de desarrollo y prueba.

En las tabla de articulo se crea un campo mas el cual sera  `user_id`, y antes de continuar crearemos usuarios en la tabla `user` 

ademas de eso vamos a crfear un archivo nuevo el cual seran los siguientes
![coamdo tree](21.jpg "comando tree")
![coamdo tree](22.jpg "comando tree")
en el esquema de articulo crearemos una relacion entre tablas y luego a exportaremos para tener los cambios
![coamdo tree](23.jpg "comando tree")
![coamdo tree](24.jpg "comando tree")
en el modelo del articulo crearemos una nueva funcion el cual se llamara user 
![coamdo tree](25.jpg "comando tree")

## 31.Many to Many Relationships With Linking Tables
A continuación, tenemos el tipo de relación "muchos a muchos", un poco más confuso. Para ilustrar esto, usaremos el ejemplo común de artículos y etiquetas. Como nos daremos cuenta rápidamente, una tercera tabla es necesaria para asociar un artículo con muchas etiquetas y una etiqueta con muchos artículos.

Creamos una nueva funcion la cual se llamara `tags`
![coamdo tree](26.jpg "comando tree")
ademas de eso crearemos la nueva tabla la cual sera `tags` la cual tendra los siguiente campos
![coamdo tree](27.jpg "comando tree")
ademas de eso crearemos la relacion entre tablas 
![coamdo tree](28.jpg "comando tree")
y ademas crearemo un modelo de `tags` donde podremos lo siguiente para que cada vez que inserte haga la relacion
![coamdo tree](29.jpg "comando tree")


## 32.Display All Tags Under Each Article
Finalmente podemos mostrar todas las etiquetas de cada artículo de la página. Además, ahora podemos filtrar todos los artículos por etiqueta.

crearemos un nuevo foreach para que recorra los `tags` y cada articulo tenga uno
![coamdo tree](30.jpg "comando tree")
asi se veria y en la parte de abajo  y cuando selecciona el que dice laravel lo llevara a otra pagina
![coamdo tree](31.jpg "comando tree")
como se puede ver en la ruta lo lleva al articulo 
![coamdo tree](32.jpg "comando tree")
ademas de eso le pediremos que cuando el nombre sea igual de vuelva junto el tag 
![coamdo tree](33.jpg "comando tree")
![coamdo tree](34.jpg "comando tree")
![coamdo tree](35.jpg "comando tree")
ademas haremos un cambio en el index y caambiaremos el foreach a un forelse  como lo muestra lo siguiente
![coamdo tree](36.jpg "comando tree")
y si no esta mostrara el siguiente mensaje 
![coamdo tree](37.jpg "comando tree")
ademas en la vista de articulo modificaremos la ruta de la etiqueta a con lo siguiente
![coamdo tree](38.jpg "comando tree")
y cada vez que precione lo llevara al siguiente parte
![coamdo tree](40.jpg "comando tree")

## 33.Attach and Validate Many-to-Many Inserts

Ahora entendemos cómo buscar y mostrar registros de una tabla de vinculación. A continuación, aprendamos cómo realizar inserciones. Podemos aprovechar los métodos attach () y detach () para insertar uno o varios registros a la vez. Sin embargo, también debemos realizar la validación necesaria para asegurarnos de que un usuario malintencionado no entre una identificación no válida.

En el formulario de crear de articulos, vamos agregarle un select para mostrar los tags
![coamdo tree](41.jpg "comando tree")
y ademas de es en la parte crear de fuction, haremos que nos traigan todos los tags
![coamdo tree](42.jpg "comando tree")
y se deberia de ver asi
![coamdo tree](43.jpg "comando tree")
![coamdo tree](44.jpg "comando tree")

guardara los datos del tags en su tabla
![coamdo tree](45.jpg "comando tree")
En el controler de articulos vamos agregar en la fuction de validateArticle lo de tags 
![coamdo tree](46.jpg "comando tree")

## Laravel From The Scratch - 8 Authentication
# 34.Build a Registration System in Mere Minutes
Laravel UI, puede crear fácilmente un sistema de registro completo que incluye registros, manejo de sesiones, restablecimiento de contraseñas, confirmaciones de correo electrónico y más.

Se crea un nuevo proyecto de laravel 
Vamos autilizar la `Authentication` que trae laravel
![coamdo tree](47.jpg "comando tree")
entraremos `Login` y `Registrar`
![coamdo tree](48.jpg "comando tree")
Donde nos mostrara la siguiente pagina para la el login
![coamdo tree](49.jpg "comando tree")
esta sera la parte de registrar 
![coamdo tree](50.jpg "comando tree")

Creamos una nueva base de datos y la cual en el laravel vamos a migrar las tablas que contiene  para la parte de autenticacion, entonces a la hora de registrar una persona este lo registrara y le permitira entrar a la parte principal cuando alguien se logea y ademas se puede ver el nombre de quien se logeo.
![coamdo tree](51.jpg "comando tree")
Podemos ver la ruta  para la autenticacion si vamos a esa ruta
![coamdo tree](52.jpg "comando tree")
veremos lo que nos permite y lo que hace la autenticacion, laravel ya lo trae y si solo quitaramos el `this-middleware('auth')` y en la ruta colocamos el home nos dejaria entrar sin pedir nada
![coamdo tree](53.jpg "comando tree")
y si ahora colocamos esta ruta al principio el ya no ns mostrara la principal de laravel si no que ahora el formulario de login
![coamdo tree](54.jpg "comando tree")

en el archivo de home vamos a mostrar el nombre de la persona autenticada asi para eso hacemos lo siguiente
![coamdo tree](55.jpg "comando tree")
y si nos volvemos a la vista y refrescamos ahora deberia de salir asi.
![coamdo tree](56.jpg "comando tree")

ahora en la parte pricipal donde sale laravel validamos que si esta autenticado saldra el nombre y si no saldra el laravel
![coamdo tree](57.jpg "comando tree")
![coamdo tree](58.jpg "comando tree")
![coamdo tree](59.jpg "comando tree")
luego cambiaremos un poco el codigo para que diga mas bien 
![coamdo tree](61.jpg "comando tree")
![coamdo tree](60.jpg "comando tree")

## 35.The Password Reset Flow
Se mostrara que pasa si un usuario olvida su contraseña, es necesario realizar una serie de acciones: solicita un restablecimiento; preparamos un token único y lo asociamos con su cuenta; enviamos un correo electrónico al usuario que contiene un enlace a nuestro sitio; una vez hecho clic, validamos el token en el enlace con lo que está almacenado en la base de datos; permitimos que el usuario establezca una nueva contraseña. Afortunadamente, Laravel puede manejar todo este flujo de trabajo automáticamente.

![coamdo tree](63.jpg "comando tree")

## Laravel From The Scratch - 9 Core Concepts
# 36.Collections
Nuestro primer concepto central es el encadenamiento de colecciones. Como seguramente ya habrá aprendido, al obtener varios registros de una base de datos, se devuelve una instancia de Colección. Este objeto no solo sirve como un envoltorio alrededor de su conjunto de resultados, sino que también proporciona docenas y docenas de métodos de manipulación útiles que podrá utilizar en cada proyecto que cree.

Ahora vamos a trabjar con colecciones hay de varias amneras una es de la siguiente manera  en el cual 
 Usaremos el ayudante para crear una nueva instancia de colección a partir de la matriz, ejecutaremos la función en cada elemento y luego eliminaremos todos los elementos
![coamdo tree](64.jpg "comando tree")
y la otra sera de esta manera y esas colecciones contendran los datos de la base de datos
![coamdo tree](65.jpg "comando tree")

## 37.CSRF Attacks, With Examples
Laravel proporciona protección contra la falsificación de solicitudes entre sitios (CSRF) lista para usar, pero es posible que aún no sepa exactamente lo que eso significa. En esta lección, se motrara algunos ejemplos de cómo se ejecuta un ataque CSRF, así como de cómo Laravel protege su aplicación contra ellos.

Las falsificaciones de solicitudes entre sitios son un tipo de exploit malicioso mediante el cual se ejecutan comandos no autorizados en nombre de un usuario autenticado.

Laravel genera automáticamente un "token" CSRF para cada sesión de usuario activa administrada por la aplicación. Este token se utiliza para verificar que el usuario autenticado es el que realmente realiza las solicitudes a la aplicación


Pondremos la siguiente ruta en la el archivo principal
![coamdo tree](66.jpg "comando tree")
si abrimos una nueva aplicacion y tratamos de ejecturarla
![coamdo tree](67.jpg "comando tree")
luego vamos a la otroa t refrescamos o entramos algun otro link, este nos sacara de la seccion
![coamdo tree](68.jpg "comando tree")

## 38.Service Container Fundamentals
El contenedor de servicios de Laravel es uno de los pilares centrales de todo el marco. Antes de revisar la implementación real, primero tomemos unos minutos para construir un contenedor de servicios simple desde cero. Esto le dará una comprensión instantánea de lo que sucede bajo el capó cuando enlaza y resuelve claves.

Como se muestra acontinuacion
En la rutas cinstanciamos lo que seria el archivo de container 
![coamdo tree](69.jpg "comando tree")
el cual se hara lo siguiente
![coamdo tree](70.jpg "comando tree")
y este reotarnara la siguiente colecccion de datos
![coamdo tree](71.jpg "comando tree")

## 39.Automatically Resolve Dependencies
pasamos a la implementación de Laravel. Como verá, además de lo básico, también puede, en algunos casos, construir objetos automáticamente para usted. Esto significa que puede "pedir" lo que necesita, y Laravel hará todo lo posible, utilizando la API de reflexión de PHP, para leer el gráfico de dependencia y construir lo que necesita.

Aremos lo siguiente 
![coamdo tree](72.jpg "comando tree")
para que nos devuelva lo siguiente
![coamdo tree](73.jpg "comando tree")

## 40.Laravel Facades Demystified

Ahora pasaremos a las fachadas de Laravel, que proporcionan una interfaz estática conveniente para todos los componentes subyacentes del marco. En esta lección, revisaremos la estructura básica, cómo rastrear la clase subyacente y cuándo puede optar por no usarlas.


![coamdo tree](74.jpg "comando tree")
la funcion de arriba con la de esta son  la misma, pero de diferentes de maneras de hacer
![coamdo tree](75.jpg "comando tree")


## 41.Service Providers are the Missing Piece
 Un proveedor de servicios es una ubicación para registrar enlaces en el contenedor y configurar su aplicación en general


El archivo `config/app.php` presente en los proyectos Laravel, contiene un arreglo llamado providers.
Este arreglo es un listado de todos los service providers que serán cargados en nuestra aplicación. Cada service provider es una clase.
No todos los proveedores son cargados cuando nuestra aplicación resuelve una petición. Muchos de ellos son cargados únicamente cuando se requieren.
 ![coamdo tree](76.jpg "comando tree")
![coamdo tree](77.jpg "comando tree")
Un service provider es un proveedor de servicios, y como tal se encarga de registrar nuestros servicios ante el service container.
Si un servicio no tiene ninguna dependencia ni se asocia a ninguna interfaz, no se necesita de ningún service provider porque la inyección de dependencias funcionará de todas formas